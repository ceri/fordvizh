#!/usr/bin/env bash
php yii migrate/up --migrationPath=@vendor/dektrium/yii2-user/migrations --interactive=0
php yii migrate/up --migrationPath=@yii/rbac/migrations --interactive=0
php yii migrate/up --migrationPath=vendor/yiisoft/yii2/rbac/migrations
php yii migrate/up --interactive=0