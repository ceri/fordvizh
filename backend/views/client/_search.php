<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\ClientSearch */
/* @var $form yii\widgets\ActiveForm */
/* @var $clientForm app\models\form\clientForm */
?>

<div class="clientclient-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($clientForm, 'id') ?>

    <?= $form->field($clientForm, 'first_name') ?>

    <?= $form->field($clientForm, 'patronymic') ?>

    <?= $form->field($clientForm, 'last_name') ?>

    <?= $form->field($clientForm, 'age') ?>

    <div class="form-group">
        <?= Html::submitButton('Поиск', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
