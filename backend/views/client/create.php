<?php

use yii\helpers\Html;
use app\models\form\ClientForm;
use app\models\Clientclient;
use app\models\search\ClientSearch;
use app\models\ClientPhone;
use app\models\search\PhoneSearch;
use yii\web\Controller;

/* @var $this yii\web\View */
/* @var $model app\models\Clientclient */

$this->title = 'Создание клиента';
$this->params['breadcrumbs'][] = ['label' => 'Clientclients', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="clientclient-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', ['clientForm' => $clientForm,]) ?>


</div>
