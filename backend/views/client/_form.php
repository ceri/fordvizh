<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $clientForm app\models\form\Clientclient */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="client-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $clientForm->errorSummary($form); ?>

    <?= $form->field($clientForm->Clientclient, 'first_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($clientForm->Clientclient, 'patronymic')->textInput(['maxlength' => true]) ?>

    <?= $form->field($clientForm->Clientclient, 'last_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($clientForm->Clientphone, 'phone_digital')->widget( MaskedInput::class, ['mask' => '+7 (999)999-99-99', 'clientOptions' => ['removeMaskOnSubmit' => true]]) ->textInput(['maxlength' => true])?>

    <?= $form->field($clientForm->Clientclient, 'age')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
