<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "client_phone".
 *
 * @property int $id
 * @property int $client_id
 * @property string $phone_digital
 *
 * @property ClientClient $client
 */
class ClientPhone extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'client_phone';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['client_id'], 'integer'],
            [['phone_digital'], 'string', 'max' => 255],
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Clientclient::class, 'targetAttribute' => ['client_id' => 'id']],
            [['phone_digital'], 'trim'],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'client_id' => 'Client ID',
            'phone_digital' => 'Phone Digital',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getclientClient(){
        return $this->hasOne(clientclient::class, ['id' => 'client_id', ]);
    }
    public function getphone_digital(){
        return $this->hasOne(clientlient::class, ['id' => 'phone_digital']);
    }
}
