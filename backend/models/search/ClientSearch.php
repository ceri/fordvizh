<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Clientclient;
use app\models\ClientPhone;

/**
 * ClientSearch represents the model behind the search form of `app\models\Clientclient`.
 */
class ClientSearch extends Clientclient
{
    public $phone_digital;
    public function rules(){
        return [
            [['id', 'age'], 'integer'],
            [['first_name', 'phone_digital', 'patronymic', 'last_name'], 'safe'],
        ];
    }
    public function scenarios(){
        return Model::scenarios();
    }
    public function search($params){
        $query = Clientclient::find()->joinWith('phone_Digital');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => array_merge($dataProvider->getSort()->attributes, [
                'phone_digital' => [
                    'asc' => [ClientPhone::tableName() . '.phone_digital' => SORT_ASC],
                    'desc' => [ClientPhone::tableName() . '.phone_digital' => SORT_DESC]
                ]
            ])
        ]);

        if (!($this->load($params) && $this->validate())) {
            $query->joinWith(['phone_Digital']);
            return $dataProvider;
        }
        $query->andFilterWhere([
            'id' => $this->id,
            'age' => $this->age,
        ]);
        $query->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'patronymic', $this->patronymic])
            ->andFilterWhere(['like', 'last_name', $this->last_name]);
        $query->andFilterWhere([
            'LIKE', ClientPhone::tableName() . '.phone_digital', $this->phone_digital
        ]);
        return $dataProvider;
    }
}
