<?php
namespace app\models\form;

use app\models\ClientClient;
use app\models\ClientPhone;
use yii;
use yii\base\model;

class ClientForm extends model
{
	public $_client;
	public $_phone;

    public function rules()
    {
        return [
            [['ClientClient'], 'required'],
            [['ClientPhone'], 'safe'],
        ];
	}
	public function afterValidate()
	{
		$error = false;
		if (!$this->clientClient->validate()) {
			 $error = true;
        }
        if (!$this->ClientPhone->validate()) {
            $error = true;
        }
        if ($error) {
            $this->addError(null);
        }
        parent::afterValidate();
	}
    public function save()
    {
        if (!$this->validate()) {
            return false;
        }
        $transaction = Yii::$app->db->beginTransaction();
        if (!$this->clientClient->save()) {
            $transaction->rollBack();
            return false;
        }
        $this->ClientPhone->client_id = $this->clientClient->id;
        if (!$this->ClientPhone->save(false)) {
            $transaction->rollBack();
            return false;
        }
        $transaction->commit();
        return true;
    }
    public function getClientclient()
    {
        return $this->_client;
    }
    public function setClientclient($client)
    {
        if ($client instanceof Clientclient) {
            $this->_client = $client;
        } else if (is_array($client)) {
            $this->_client->setAttributes($client);
        }
    }
    public function getClientphone()
    {
        if ($this->_phone === null) {
            if ($this->Clientclient->isNewRecord) {
                $this->_phone = new ClientPhone();
                $this->_phone->loadDefaultValues();
            } else {
                $this->_phone = $this->ClientClient->clientPhone;
            }
        }
        return $this->_phone;
    }
    public function setClientPhone($ClientPhone)
    {
        if (is_array($ClientPhone)) {
            $this->Clientphone->setAttributes($ClientPhone);
        } elseif ($ClientPhone instanceof ClientPhone) {
            $this->_phone = $ClientPhone;
        }
    }
    public function errorSummary($form)
    {
        $errorLists = [];
        foreach ($this->getAllModels() as $id => $model) {
            $errorList = $form->errorSummary($model, [
                'header' => '<p>Please fix the following errors for <b>' . $id . '</b></p>',
            ]);
            $errorList = str_replace('<li></li>', '', $errorList); // remove the empty error
            $errorLists[] = $errorList;
        }
        return implode('', $errorLists);
    }
    private function getAllModels()
    {
        return [
            'ClientClient' => $this->clientclient,
            'ClientPhone' => $this->ClientPhone,
        ];
    }
}