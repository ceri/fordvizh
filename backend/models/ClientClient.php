<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "client_client".
 *
 * @property int $id
 * @property string $first_name
 * @property string $patronymic
 * @property string $last_nameW
 * @property int $age
 *
 * @property ClientPhone[] $clientPhones
 */
class ClientClient extends \yii\db\ActiveRecord
{
    public static function tableName(){
        return 'client_client';
    }
    public function rules(){
        return [
            [['age'], 'integer'],
            [['first_name', 'last_name', 'age'], 'required'],
            [['first_name', 'patronymic', 'last_name'], 'string', 'max' => 255],
            
        ];
    }
    public function attributeLabels(){
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'patronymic' => 'Patronymic',
            'last_name' => 'Last Name',
            'age' => 'Age',
        ];
    }
    public function getClientPhone(){
        return $this->hasOne(ClientPhone::class, ['client_id' => 'id']);
    }
    public function getPhone_Digital(){
        return $this->hasOne(ClientPhone::class, ['client_id' => 'id']);
    }
}