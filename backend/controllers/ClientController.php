<?php

namespace backend\controllers;

use Yii;
use app\models\form\ClientForm;
use app\models\Clientclient;
use app\models\search\ClientSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * ClientController implements the CRUD actions for Clientclient model.
 */
class ClientController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['index','view','create','update','delete'],
                        'allow' => true,
                        'roles' => ['superadmin'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    /**
     * Lists all Clientclient models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ClientSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Clientclient model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'clientForm' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Clientclient model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $clientForm = new ClientForm();
        $clientForm->Clientclient = new ClientClient();
        $clientForm->setAttributes(Yii::$app->request->post());
        if (Yii::$app->request->post() && $clientForm->save()) {
            Yii::$app->getSession()->setFlash('success', 'Clientclient has been created.');
            return $this->redirect(['view', 'id' => $clientForm->clientClient->id]);
        } elseif (!Yii::$app->request->isPost) {
            $clientForm->load(Yii::$app->request->get());
        }
        return $this->render('create', ['clientForm' => $clientForm]);
    }

    /**
     * Updates an existing Clientclient model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $clientForm = new ClientForm();
        $clientForm->Clientclient = $this->findModel($id);
        $clientForm->setAttributes(Yii::$app->request->post());
        if (Yii::$app->request->post() && $clientForm->save()) {
            Yii::$app->getSession()->setFlash('success', 'clientClient has been created.');
            return $this->redirect(['view', 'id' => $clientForm->clientClient->id]);
        } elseif (!Yii::$app->request->isPost) {
            $clientForm->load(Yii::$app->request->get());
        }
        return $this->render('create', ['clientForm' => $clientForm]);
    }


    /**
     * Deletes an existing Clientclient model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Clientclient model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Clientclient the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Clientclient::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}