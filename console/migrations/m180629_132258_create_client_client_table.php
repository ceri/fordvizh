<?php

use yii\db\Migration;

/**
 * Handles the creation of table `client_client`.
 */
class m180629_132258_create_client_client_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('client_client', [
            'id' => $this->primaryKey(),
            'first_name' => $this->string()->notNull(),
            'patronymic' => $this->string(),
            'last_name' => $this->string()->notNull(),
            'age' => $this->integer(100)->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('client_client');
    }
}
