<?php

use yii\db\Migration;

/**
 * Handles the creation of table `client_phone`.
 */
class m180629_132327_create_client_phone_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('client_phone', [
            'id' => $this->primaryKey(),
            'client_id' => $this->integer(),
            'phone_digital' => $this->string(10)->notNull(),
        ]);

        $this->addForeignKey(
            'phone_to_client',
            'client_phone',
            'client_id',
            'client_client',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('client_phone');
    }
}
