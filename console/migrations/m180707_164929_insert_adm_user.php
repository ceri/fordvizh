<?php

use yii\db\Migration;

/**
 * Class m180707_164929_insert_adm_user
 */
class m180707_164929_insert_adm_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up(){
        $this->insert('user',[
            'username'          => 'adm',
            'email'             => 'teadm@gmail.com',
            'password_hash'     => '$2y$10$owDAP8re3IzKbeCboEzKgOpUPioMHjbE4CZgZ5OoDs5SK5G8kw/SW',
            'auth_key'          => 'B1YDi4SKtb5-RehmWuB_-EbHyTuWgdLF',
            'confirmed_at'      => '1530982734',
            'created_at'        => '1530982734',
            'updated_at'        => '1530982734',
            'flags'             => '0',
            'last_login_at'     => 'NULL',
        ]);
        $this->insert('user',[
            'username'          => 'superadmin',
            'email'             => 'test@gmail.com',
            'password_hash'     => '$2y$10$kIxW7H3rqeToUKOROt3bXuYOzCqHxoisBVIXdRaY2mx8uWgtzn0LG',
            'auth_key'          => 'tswiV_rpUPMwThIIknJFrE7uaM7p9Eyd',
            'confirmed_at'      => '1530982741',
            'created_at'        => '1530982741',
            'updated_at'        => '1530982741',
            'flags'             => '0',
            'last_login_at'     => 'NULL',
        ]);
        $this->insert('user',[
            'username'          => 'user',
            'email'             => 'testa@gmail.com',
            'password_hash'     => '$2y$10$Bzr757TH.iySO.Ff8T1IluWt1Idbc8EUa0rFlyWU0Fni7xhr7lkqi',
            'auth_key'          => 'hheJ9CfgCbEyyDeAF6_uwBGdW3gqxfNh',
            'confirmed_at'      => '1530983227',
            'created_at'        => '1530983227',
            'updated_at'        => '1530983227',
            'flags'             => '0',
            'last_login_at'     => 'NULL',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down(){
        $this->delete('user', ['username' => 'adm'],['username' => 'superadmin']);
    }
}
